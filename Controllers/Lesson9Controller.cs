﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SqlEfApi.Data;
using SqlEfApi.Models;

namespace SqlEfApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Lesson9Controller : ControllerBase
    {
        private readonly EFContext _context;
        
        public Lesson9Controller(EFContext context)
        {
            _context = context;
        }

        // показать все поля
        [HttpGet("GetAllItems")]
                
        public IEnumerable<Lesson9> Get()
        {
            return _context.Lesson9;
        }

        // поиск записи по конкретному id
        [HttpGet("{id}")]
        public ActionResult<Lesson9> GetLesson9(int id)
        {
            var ls = _context.Lesson9.Find(id);

            if (ls == null)
            {
                return NotFound();
            }

            return Ok(ls);
        }

        // создание новой записи
        [HttpGet("Create")]
        public string Get(string textik)
        {
            Lesson9 lesson9 = new Lesson9();
            lesson9.Request = textik;

            lesson9.Date = DateTime.Now.ToString("dd/MM/yy");

            char[] obrtextik = textik.ToCharArray();
            Array.Reverse(obrtextik);
            string finaltextik = new string(obrtextik);

            if (textik == finaltextik)
            {
                lesson9.Result = "+";
            }
            else
            {                
                lesson9.Result = "-";
            }

            _context.Lesson9.Add(lesson9);
            _context.SaveChanges();

            return finaltextik;

        }
              

        // ручная замена полей на произвольные значения
        [HttpPut("{id}")]
        public ActionResult<Lesson9> PutLesson9(int id, Lesson9 ls)
        {
            if (id != ls.Id)
                return BadRequest();

            _context.Entry(ls).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        // удаление по id
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            Lesson9 lesson9 = _context.Lesson9.Find(id);
            if (lesson9 is null)
            {
                return "This id was not found";
            }

            _context.Lesson9.Remove(lesson9);
            _context.SaveChanges();

            return "Success";
        }


        
    }
}
