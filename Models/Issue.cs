﻿using System.ComponentModel.DataAnnotations;

namespace SqlEfApi.Models
{
    public class Issue
    {
        public int Id { get; set; }   
        [Required] // этот атрибут указывает, что свойство должно иметь значение
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public IssueType IssueType { get; set; }
        public Priority Priority { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Completed { get; set; }

    }

    public enum Priority
    {
        Low, Medium, High
    }

    public enum IssueType
    {
        Feature, Bug, Documentation
    }
}
