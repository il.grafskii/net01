﻿namespace SqlEfApi.Configuration
{
    public class ConnectionConfiguration
    {
        public string SqlEfApiConnection { get; set; }
    }
}
