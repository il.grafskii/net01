﻿using Microsoft.EntityFrameworkCore;
using SqlEfApi.Models;

namespace SqlEfApi.Data
{
    public class EFContext : DbContext
    {
        public EFContext(DbContextOptions dbContextOptions) : base(dbContextOptions) { }

        public DbSet<Lesson9> Lesson9 { get; set; }
        
    }
}
